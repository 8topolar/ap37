(function script() {
  'use strict';
  var w, h;

  var config = {
    deviceName: '8to',
    textSize: 13,
    color: {
      primary: '#0c0',
      hover: '#0f0',
      background: [
        '#335533',
        '#116611'
      ]
    },
    apps: []
  }

  function init() {
    ap37.setTextSize(config.textSize);

    w = ap37.getScreenWidth();
    h = ap37.getScreenHeight();

    background.init();
    print(3, 1, config.deviceName);
    time.init();
    battery.init();
    apps.init();
    volumeControl.init()
    print(w - 3, h - 1, 'EOF');

    ap37.setOnTouchListener(function (x, y) {
      apps.onTouch(x, y);
      lineGlitch.onTouch(x, y);
      wordGlitch.onTouch(x, y);
      volumeControl.onTouch( x, y )
    });
  }

  // modules

  var background = {
    buffer: [],
    bufferColors: [],
    pattern: '',
    printPattern: function (x0, xf, y) {
      print(x0, y,
        background.pattern.substring(y * w + x0, y * w + xf),
        config.color.background[1]);
    },
    init: function () {
      background.pattern = rightPad(script, h * w, ' ');

      for (var i = 0; i < h; i++) {
        background.buffer.push(background.pattern.substr(i * w, w));
        background.bufferColors.push(arrayFill(config.color.background[1], w));
      }

      ap37.printLines(background.buffer, config.color.background[0]);
    }
  };

  var time = {
    update: function () {
      var d = ap37.getDate();
      var time = d.year +
        leftPad(d.month, 2, '0') + leftPad(d.day, 2, '0') + ' ' +
        leftPad(d.hour, 2, '0') + leftPad(d.minute, 2, '0');
      print(w - 15, 1, time);
    },
    init: function () {
      time.update();
      setInterval(time.update, 60000);
    }
  };

  var battery = {
    update: function () {
      print(w - 19, 1,
        leftPad(ap37.getBatteryLevel(), 3, ' '));
    },
    init: function () {
      battery.update();
      setInterval(battery.update, 60000);
    }
  };

  var apps = {
    list: [],
    lineHeight: 2,
    topMargin: 3,
    bottomMargin: 12,
    lines: 0,
    appWidth: 12,
    appsPerLine: 0,
    appsPerPage: 0,
    currentPage: 0,
    isNextPageButtonVisible: false,
    printPage: function (page) {
      var appPos = page * apps.appsPerPage;

      for (var x = 0; x + apps.appWidth <= w; x += apps.appWidth) {
        for (var y = apps.topMargin; y < apps.topMargin + apps.lines *
        apps.lineHeight; y += apps.lineHeight) {
          background.printPattern(x, x + apps.appWidth, y);
          if (appPos < apps.list.length) {
            var app = apps.list[appPos];
            app.y = y;
            app.x0 = x;
            app.xf = x + apps.appWidth;
            apps.printApp(app, false);
            appPos++;
          }
        }
      }
    },
    printApp: function (app, highlight) {
      let name = app.alias || app.name
      print(app.x0, app.y, '_' +
        name.substring(0, apps.appWidth - 2),
        highlight ? config.color.hover : app.color ? app.color : config.color.primary);
      if (highlight) {
        setTimeout(function () {
          apps.printApp(app, false);
        }, 1000);
      } else {
        print(app.x0 + 1, app.y, name.substring(0, 1), app.color ? app.color : config.color.hover);
      }
    },
    init: function () {
      apps.list = ap37.getApps();

      // map apps config
      apps.list = apps.list.reduce( ( result, item ) => {
        let itemConf = config.apps.filter( el => ( el.name === item.name ) ).shift()

        if ( itemConf ) {
          let newItem = { ...item, ...itemConf }
          if ( !newItem.hidden )
            result.push( newItem )
        } else
          result.push( item )

        return result
      }, [] )

      apps.lines = Math.floor(
        (h - apps.topMargin - apps.bottomMargin) / apps.lineHeight);
      apps.appsPerLine = Math.ceil(apps.list.length / apps.lines);
      apps.appWidth = Math.floor(w / apps.appsPerLine);

      // check minimum app name length
      if (apps.appWidth < 6) {
        apps.appWidth = 6;
        apps.appsPerLine = Math.floor(w / apps.appWidth);
        apps.isNextPageButtonVisible = true;
        print(w - 4, h - 9, '>>>');
        print(w - 4, h - 8, '>>>');
      } else {
        apps.isNextPageButtonVisible = false;
        background.printPattern(w - 4, w, h - 9);
      }

      apps.appsPerPage = apps.lines * apps.appsPerLine;
      apps.currentPage = 0;

      apps.printPage(apps.currentPage);

      ap37.setOnAppsListener(apps.init);
    },
    onTouch: function (x, y) {
      for (var i = apps.currentPage * apps.appsPerPage; i <
      apps.list.length; i++) {
        var app = apps.list[i];
        if (y >= app.y && y <= app.y + 1 &&
          x >= app.x0 && x <= app.xf) {
          apps.printApp(app, true);
          ap37.openApp(app.id);
          return;
        }
      }
      if (apps.isNextPageButtonVisible &&
        y >= h - 9 && y <= h - 8 &&
        x >= w - 4 && x <= w) {
        apps.currentPage++;
        if (apps.currentPage * apps.appsPerPage >= apps.list.length) {
          apps.currentPage = 0;
        }
        apps.printPage(apps.currentPage);
      }
    }
  };

  var volumeControl = {
    position: {
      x: 0,
      y: 0
    },
    printControl: function() {
      const { position } = volumeControl

      print( position.x - 1, position.y, 'vol' )
      print( position.x, position.y + 2, '+' )
      print( position.x, position.y + 4, '-' )
    },
    init: function() {
      var it = volumeControl
      it.position = {
        x: 4,
        y: h - 11
      }
      volumeControl.printControl()
    },
    onTouch: function( x, y ) {
      const { position } = volumeControl
      const xLimit = {
        low: position.x,
        high: position.x + 2,
      }
      if ( x >= xLimit.low && x <= xLimit.high ) {
        if ( y === position.y + 2 ) {
          ap37.openLink( 'http://192.168.0.136:4154/high' );
        }
        if ( y === position.y + 4 ) {
          ap37.openLink( 'http://192.168.0.136:4154/low' );
        }
      }
    }
  }

  var wordGlitch = {
    tick: 0,
    length: 0,
    x: 0,
    y: 0,
    text: [],
    active: false,
    intervalId: null,
    update: function () {
      var g = wordGlitch;
      if (g.tick === 0) { // generate new glitch
        g.length = 5 + Math.floor(Math.random() * 6);
        g.x = Math.floor(Math.random() * (w - g.length));
        g.y = Math.floor(Math.random() * h);

        g.text = [];
        for (var i = 0; i < 5; i++) {
          g.text.push(Math.random().toString(36).substr(2, g.length));
        }

        ap37.print(g.x, g.y, g.text[g.tick], config.color.background[1]);
        g.tick++;
      } else if (g.tick === 5) { // remove glitch
        ap37.printMultipleColors(g.x, g.y,
          background.buffer[g.y].substr(g.x, g.length),
          background.bufferColors[g.y].slice(g.x, g.x + g.length)
        );
        g.tick = 0;
        if (!wordGlitch.active) {
          clearInterval(wordGlitch.intervalId);
        }
      } else {
        ap37.print(g.x, g.y, g.text[g.tick], config.color.background[1]);
        g.tick++;
      }
    },
    onTouch: function (x, y) {
      if (x > w - 6 && y > h - 4) {
        wordGlitch.active = !wordGlitch.active;
        if (wordGlitch.active) {
          wordGlitch.intervalId = setInterval(wordGlitch.update, 100);
        }
      }
    }
  };

  var lineGlitch = {
    tick: 0,
    line: 0,
    active: false,
    intervalId: null,
    update: function () {
      var g = lineGlitch;
      if (g.tick === 0) { // shift line
        g.line = 1 + Math.floor(Math.random() * h - 1);

        var offset = 1 + Math.floor(Math.random() * 4),
          direction = Math.random() >= 0.5;

        if (direction) {
          ap37.printMultipleColors(0, g.line,
            rightPad(
              background.buffer[g.line].substring(offset), w,
              ' '),
            background.bufferColors[g.line].slice(offset));
        } else {
          ap37.printMultipleColors(0, g.line,
            leftPad(background.buffer[g.line]
              .substring(0, w - offset), w, ' '),
            arrayFill(config.color.hover, offset)
              .concat(background.bufferColors[g.line]
                .slice(0, w - offset))
          );
        }
        g.tick++;
      } else { // restore line
        ap37.printMultipleColors(
          0, g.line, background.buffer[g.line],
          background.bufferColors[g.line]);
        g.tick = 0;
        if (!lineGlitch.active) {
          clearInterval(lineGlitch.intervalId);
        }
      }
    },
    onTouch: function (x, y) {
      if (x > w - 6 && y > h - 4) {
        lineGlitch.active = !lineGlitch.active;
        if (lineGlitch.active) {
          lineGlitch.intervalId = setInterval(lineGlitch.update, 200);
        }
      }
    }
  };

  //utils

  function print(x, y, text, color) {
    color = color || config.color.hover;
    background.buffer[y] = background.buffer[y].substr(0, x) + text +
      background.buffer[y].substr(x + text.length);
    for (var i = x; i < x + text.length; i++) {
      background.bufferColors[y][i] = color;
    }
    ap37.print(x, y, text, color);
  }

  function get(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onload = function () {
      if (xhr.status === 200) {
        callback( null, xhr.response )
      } else {
        debug( 'else ' + xhr.status )
        error( xhr.response )
      }
    };
    xhr.onerror = ( err ) => {
      debug( 'err ' + url + ' ' + xhr.status )
      debug( xhr )
      error( err )
    }
    xhr.send();
  }

  function leftPad(str, newLength, char) {
    str = str.toString();
    return newLength > str.length ?
      new Array(newLength - str.length + 1).join(char) + str : str;
  }

  function rightPad(str, newLength, char) {
    str = str.toString();
    return newLength > str.length ?
      str + new Array(newLength - str.length + 1).join(char) : str;
  }

  function arrayFill(value, length) {
    var result = [];
    for (var i = 0; i < length; i++) {
      result.push(value);
    }
    return result;
  }

  function require( cdn ) {
    return new Promise( function( resolve, reject ) {
      let lib = document.createElement( 'script' )
      lib.src = cdn

      lib.onload = function( a ) {
        return resolve( a )
      }

      lib.onreadystatechange = function( a ) {
        return resolve( a )
      }

      lib.onerror = function( a ) {
        return reject( a )
      }

      document.head.appendChild( lib )
    } )
  }

  async function fetch( url ) {
    return new Promise( ( resolve, reject ) => {
      var xhr = new XMLHttpRequest()
      xhr.open('GET', url, true)
      xhr.onload = function () {
        if (xhr.status === 200) {
          resolve(xhr.response)
        }
      }
      xhr.onerror = reject
      xhr.send()
    } )
  }

  function debug( message ) {
    if ( message instanceof Error )
      message = message.toString()
    if ( typeof message === 'object' )
      message = JSON.stringify( message )
    print( 0, h - 4, message )
  }

  function error( message ) {
    if ( message instanceof Error )
      message = message.toString()
    if ( typeof message === 'object' )
      message = JSON.stringify( message )
    print( 0, h - 2, message, '#f00' )
  }

  // require( 'https://cdn.jsdelivr.net/npm/ipfs-http-client/dist/index.min.js' )
  //   .then( ( a ) => {
  //     debug( 'loaded' )
  //     const ipfs = IpfsHttpClient( 'http://127.0.0.1:5001' )
  //     return ipfs.id()
  //   } )
  //   .then( identity => {
  //     debug( identity )
  //   } )
  //   .catch( debug )

  fetch( 'https://gitlab.com/8topolar/ap37/-/raw/master/config.json' )
    .then( response => {
      const conf = JSON.parse( response )
      config = { ...config, ...conf }
      init()
    } )
    .catch( debug )

})();

// pull requests github.com/apseren/ap37
